<?php

namespace App;

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
}, 20);
