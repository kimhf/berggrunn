<?php

namespace App;

/**
 * Determine if the stack has any content pushed to it.
 *
 * @param  string $stack
 * @return boolean
 */
function stack_has_content(string $stack) : bool
{
    return (boolean) trim(sage('blade')->yieldPushContent($stack));
}

/**
 * Determine if the section has any content pushed to it.
 *
 * DEPRECATED Use:
 *
 * @hasSection('sectionname')
 *  content
 * @endif
 *
 * @param  string $stack
 * @return boolean
 */
function section_has_content(string $section) : bool
{
    return (boolean) sage('blade')->hasSection($section);
}
