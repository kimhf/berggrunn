import { detectIE } from './../pollyfills/tests'

/**
 * Set Page Header Height
 */

let timeoutID
let config = []
let pageWidth = 0
let pageHeight = 0
let initDone = false

const init = () => {
  if (!initDone) {
    initDone = true

    window.addEventListener('resize', browserResize)
  }
}

const evaluate = () => {
  if (pageWidth !== window.innerWidth || (pageHeight > (window.innerHeight + 75) || pageHeight < (window.innerHeight - 75))) {
    pageWidth = window.innerWidth
    pageHeight = window.innerHeight

    var height = window.innerHeight

    config.forEach((settings) => {
      const targetHeight = settings.height(settings.elements)

      let newMinHeight = (height / 100 * targetHeight)

      if (settings.subtract) {
        newMinHeight = newMinHeight - settings.subtract(settings.elements)
      }

      if (newMinHeight && newMinHeight > 150) {
        if (newMinHeight < 175) {
          newMinHeight = 175
        }

        let totalHeight = 0

        settings.elements.children().each((i, child) => {
          totalHeight = totalHeight + $(child).outerHeight(true)
        })

        let property = 'min-height'
        if (detectIE() && totalHeight < newMinHeight) {
          // Fix vertical centering of elements with flexbox.
          property = 'height'
        }

        settings.elements.css(property, newMinHeight)
      }
    })
  }
}

const browserResize = () => {
  clearTimeout(timeoutID)

  timeoutID = window.setTimeout(evaluate, 250)
}

export default function (settings) {
  if (settings.elements.length) {
    config.push(settings)
  }

  init()
  browserResize()
}
