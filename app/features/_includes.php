<?php

namespace App;

/**
 * Required features files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($feature) {
    $file = __DIR__ . "/{$feature}.php";
    if (!file_exists($file)) {
        sage_error(sprintf(__('Error locating feature <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    } else {
        require_once($file);
    }
}, [
    'logo',
    'svg_uploads'
]);
