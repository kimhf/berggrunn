import jQuery from 'jquery'

let isInitialized = false

export default function () {
  if (!isInitialized) {
    jQuery(document).on('paste', 'textarea.autoExpand, .gfield.autoExpand textarea', autoExpand)
    jQuery(document).on('input', 'textarea.autoExpand, .gfield.autoExpand textarea', autoExpand)
    jQuery(document).on('keyup', 'textarea.autoExpand, .gfield.autoExpand textarea', autoExpand)

    window.addEventListener('load', expandAll)
    window.addEventListener('resize', expandAll)
    jQuery(document).on('gform_post_render', expandAll)

    isInitialized = true
  }
}

function autoExpand (event) {
  const el = event.target || event.srcElement
  if (!el) {
    return
  }
  autoExpandElement(el)
}

function autoExpandElement (el) {
  const scrollTop = document.documentElement.scrollTop

  el.style.overflow = 'hidden'
  el.style.height = 'inherit'
  el.style.height = (el.scrollHeight + 2) + 'px'

  document.documentElement.scrollTop = scrollTop

  el.style.overflow = 'auto'
}

function expandAll () {
  var tag = document.querySelectorAll('textarea.autoExpand, .gfield.autoExpand textarea')
  for (var i = 0; i < tag.length; i++) {
    // @ts-ignore
    tag[i].style.resize = 'none'

    // @ts-ignore
    tag[i].rows = 3

    autoExpandElement(tag[i])
  }
}
