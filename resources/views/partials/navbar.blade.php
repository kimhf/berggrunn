<header class="banner {{$banner_classes}}">
  <nav class="navbar {{$navbar_classes}}">
    {!!$navbar_inner_container['before']!!}
      <div class="navbar-main-section">
        @php(the_custom_logo())

        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarTogglerMenu" aria-controls="navbarTogglerMenu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse d-lg-flex justify-content-lg-end" id="navbarTogglerMenu">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav mt-2 mt-md-0', 'container' => '', 'echo' => true, 'walker' => new App\Bootstrap_Nav_Walker()]) !!}
        @endif
      </div>

    {!!$navbar_inner_container['after']!!}
  </nav>
</header>
