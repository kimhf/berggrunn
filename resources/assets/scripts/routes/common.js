import jQuery from 'jquery'
import objectFitImages from 'object-fit-images'
import onAnchorHashClick from './../lib/onAnchorHashClick'
import setElementMinHeight from './../lib/setElementMinHeight'
import mediaQuery from './../util/mediaQuery'

export default {
  init () {
    // JavaScript to be fired on all pages

    jQuery('.menu-item a[href*="#"]:not([href="#"])').on('click', onAnchorHashClick)

    // Init object-fit pollyfill
    jQuery('img.object-fit-image:not(.lazy-hidden), .object-fit-image img:not(.lazy-hidden)').each(objectFitImages)
    jQuery('img.object-fit-image.lazy-hidden, .object-fit-image img.lazy-hidden').on('lazyloaded', objectFitImages)
  },
  finalize () {
    // JavaScript to be fired on all pages, after page specific JS is fired

    jQuery(window).on('load', () => {
      /*
      setElementMinHeight({
        elements: jQuery('.min-h-cover'),
        height: (_) => 100,
        subtract: () => {
          let sub = subtractElement.innerHeight()
          if (sub > 200) {
            sub = 200
          }
          return sub
        },
      })
      */

      const mediaBreakpointUpMd = () => {
        return mediaQuery('(min-width: 768px)')
      }

      const subtractElement = $('.banner:not(.fixed-top):not(.absolute-top)')

      jQuery('.min-h-cover, .min-h-100, .min-h-50, .min-h-md-50, .min-h-md-100').each(function (i, elements) {
        setElementMinHeight({
          elements: jQuery(elements),
          height: (element) => {
            const upMd = mediaBreakpointUpMd()
            let h = 100

            if (upMd && element.hasClass('min-h-md-50')) {
              h = 50
            } else if (upMd && element.hasClass('min-h-md-100')) {
              h = 100
            } else if (element.hasClass('min-h-cover')) {
              h = 100
            } else if (element.hasClass('min-h-100')) {
              h = 100
            } else if (element.hasClass('min-h-50')) {
              h = 50
            }

            return h
          },
          subtract: (element) => {
            const upMd = mediaBreakpointUpMd()
            let sub = 0

            if (element.hasClass('min-h-cover')) {
              if (upMd && (element.hasClass('min-h-md-50') || element.hasClass('min-h-md-100'))) {

              } else {
                sub = subtractElement.innerHeight()
                if (sub > 200) {
                  sub = 200
                }
              }
            }

            return sub
          },
        })
      })

      /*
      setElementMinHeight({
        elements: jQuery('.min-h-50'),
        height: () => {
          return 50
        },
      })

      setElementMinHeight({
        elements: jQuery('.min-h-50'),
        height: 50,
      })
      */
    })
  },
}
