<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), get_main_style_dependencies(), null);
    wp_add_inline_style('sage/main.css', get_main_inline_styles());

    wp_register_script('sage/main.js', asset_path('scripts/main.js'), ['jquery', 'berggrunn/scripts/bootstrap'], null, true);
    wp_localize_script('sage/main.js', 'SAGE_DIST_PATH', trailingslashit(config('assets.uri')));
    wp_enqueue_script('sage/main.js');
}, 100);

add_action('admin_enqueue_scripts', function () {
    // wp_enqueue_style('sage/admin-style.css', asset_path('styles/admin-style.css'), get_main_style_dependencies(), null);
    // wp_add_inline_style('sage/admin-style.css', get_main_inline_styles());
    // wp_add_inline_style('sage/admin-style.css', wp_custom_css_cb());
});

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Add gutenberg support
     */
    add_theme_support( 'editor-color-palette', [
        [
            'name' => __( 'primary', 'sage' ),
            'slug' => 'primary',
            'color' => get_theme_mod('primary_color', config('styles.primary_color')),
        ],
        [
            'name' => __( 'secondary', 'sage' ),
            'slug' => 'secondary',
            'color' => get_theme_mod('secondary_color', config('styles.secondary_color')),
        ],
        [
            'name' => __( 'muted', 'sage' ),
            'slug' => 'muted',
            'color' => '#6c757d',
        ],
        [
            'name' => __( 'dark', 'sage' ),
            'slug' => 'dark',
            'color' => '#343a41',
        ],
    ] );
    add_theme_support('align-wide');

    /*
    add_theme_support('gutenberg');
    add_theme_support(
        'editor-color-palette',
        '#a156b4',
        '#d0a5db',
        '#eee',
        '#444'
    );
    */

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Enable support for custom logo. The size is a @2x retina ready size, and will be displayed at half this size.
     */
    add_theme_support('custom-logo', [
        'height'      => 800,
        'width'       => 1024,
        'flex-height' => true,
        'flex-width'  => false,
        'header-text' => ['site-title', 'site-description'],
    ]);

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));

    /**
     * Make theme available for translation.
     */
    load_theme_textdomain('sage', config('assets.languages'));

    /**
     * Add image sizes specific for berggrunn
     */
    add_image_size('berggrunn-featured-image-full', 2000, 1125, true);
    add_image_size('berggrunn-featured-image-wide', 1200, 674, true);

    /**
     * Add theme support for custom backgrounds.
     */
    add_theme_support('custom-background', ['default-color' => 'ffffff']);
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $sidebars = [
        [
            'name'          => __('Primary', 'sage'),
            'id'            => 'sidebar-primary'
        ],[
            'name'           => __('Footer', 'sage'),
            'id'             => 'sidebar-footer',
            'bootstrap_grid' => true
        ]
    ];

    foreach ($sidebars as $sidebar) {
        register_sidebar($sidebar + [
            'before_widget' => '<section class="widget ' . $sidebar["id"] . '__widget %1$s %2$s ' . $sidebar["id"] . '__%2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3 class="widget-title ' . $sidebar["id"] . '__widget-title">',
            'after_title'   => '</h3>'
        ]);
    }
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });
});
