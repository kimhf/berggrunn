<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class()){!!$body_anchor!!}>
    @php(do_action('get_header'))
    @include('partials.navbar')
    <div class="wrap" role="document">
      @yield('content')
    </div>
    @php(do_action('get_footer'))
    @include('partials.footer')
    @php(wp_footer())
  </body>
</html>
