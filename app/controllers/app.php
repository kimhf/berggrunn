<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName(): string
    {
        return get_bloginfo('name');
    }

    public function navbarInnerContainer(): array
    {
        $container = [
            'before' => '',
            'after' => ''
        ];

        $theme_mod = \App\get_contextual_theme_mod('navbar__inner_container');

        if ($theme_mod && $theme_mod !== 'none') {
            $class = esc_attr($theme_mod);
            $container['before'] = "<div class='{$class}'>";
            $container['after'] = "</div>";
        }

        return $container;
    }

    public function bodyAnchor(): string
    {
        $navbar__anchor = \App\get_contextual_theme_mod('navbar__anchor');

        if ($navbar__anchor) {
            return ' id="' . $navbar__anchor . '"';
        }

        return '';
    }

    public function bannerClasses(): string
    {
        $theme_mod = \App\get_contextual_theme_mod('navbar__placement');

        $navbarClasses = [];

        if ($theme_mod) {
            $navbarClasses[] = $theme_mod;
        }

        return implode(' ', $navbarClasses);
    }

    public function navbarClasses(): string
    {
        $theme_mods = \App\get_contextual_theme_mods();

        $navbarClasses = ['navbar-expand-lg'];

        /*
        if ($theme_mods['navbar__placement']) {
            $navbarClasses[] = $theme_mods['navbar__placement'];
        }
        */

        if ($theme_mods['navbar__color']) {
            $navbarClasses[] = $theme_mods['navbar__color'];
        }

        if ($theme_mods['navbar__background']) {
            $navbarClasses[] = $theme_mods['navbar__background'];
        }

        return implode(' ', $navbarClasses);
    }

    public function footerClass(): string
    {
        $footer__class = \App\get_contextual_theme_mod('footer__class');

        if ($footer__class) {
            $footer__class = trim($footer__class);
            $footer__class = preg_replace("/\s+/", " ", $footer__class);
            return ' ' . esc_attr($footer__class);
        }

        return '';
    }

    public function showPageHeader(): bool
    {
        $page_header__visability = \App\get_contextual_theme_mod('page_header__visability');

        if ($page_header__visability === 'hidden') {
            return false;
        }

        return true;
    }

    public static function title(): string
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
}
