@extends('layouts.app')

@section('content')
  <div class="content">
    <main class="main container-main">
      @while(have_posts()) @php(the_post())
        @include('partials.content-single-'.get_post_type())
      @endwhile
    </main>
    @if (App\display_sidebar())
      <aside class="sidebar container-sidebar">
        @include('partials.sidebar')
      </aside>
    @endif
  </div>
@endsection
