<?php

namespace App;

class Excerpt
{
    /**
     * Instance of the class.
     *
     * @since   1.0
     *
     * @var   object
     */
    protected static $instance = null;

    /**
     * Default excerpt length.
     *
     * @since   1.0
     *
     * @var   int
     */
    private static $default_length = 0;

    /**
     * Custom excerpt length.
     *
     * @since   1.0
     *
     * @var   int
     */
    private static $custom_length = 0;

    /**
     * Initialize the class by setting up action hooks.
     *
     * @access private
     * @since 1.0
     */
    private function __construct()
    {
        add_action('excerpt_more', [ $this, 'excerpt_more' ], 999);
        add_action('excerpt_length', [ $this, 'excerpt_length' ], 999);
        // add_filter('get_the_excerpt', [ $this, 'get_the_excerpt'], 10, 1);
    }

    /* function get_the_excerpt($output)
    {
        $output = str_replace('&nbsp;', ' ', $output);
        $output = preg_replace('/\s+/', ' ', $output);
        //var_pump($output);
        return $output;
    }*/

    /**
     * Add "… Continued" to the excerpt
     */
    public static function excerpt_more() : string
    {
        return ' &hellip;';
        // return ' &hellip; <a href="' . get_permalink() . '">' . __('Read more', 'sage') . '</a>';
    }

    public static function excerpt_length(int $default_length) : int
    {
        self::$default_length = $default_length;
        if (self::$custom_length) {
            return self::$custom_length;
        }
        return $default_length;
    }

    /**
     * Set the excerpt length.
     *
     * @access public
     * @since 1.0
     */
    public static function set_length(int $length)
    {
        self::$custom_length = (int)$length;
    }

    /**
     * Reset the excerpt length.
     *
     * @access public
     * @since 1.0
     */
    public static function reset_length()
    {
        self::$custom_length = 0;
    }

    /**
     * Return an instance of this class.
     *
     * @access public
     * @since 1.0
     * @return object  A single instance of the class.
     */
    public static function get_instance()
    {

        // If the single instance hasn't been set yet, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
