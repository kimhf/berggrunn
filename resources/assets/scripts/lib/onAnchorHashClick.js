/**
 * Handles a anchor click event.
 * Do a smooth scroll to the document element when they are found on the current page.
 */
export default function (event) {
  // @ts-ignore
  const pathname = this.pathname
  // @ts-ignore
  const hostname = this.hostname
  // @ts-ignore
  const search = this.search

  if (location.pathname.replace(/^\//, '').replace(/\/$/, '') === pathname.replace(/^\//, '').replace(/\/$/, '') && location.hostname === hostname && location.search === search) {
    // @ts-ignore
    const href = this.href

    if (href) {
      const hash = href.split('#')[1]
      const element = document.getElementById(hash)

      if (element && typeof element.scrollIntoView !== 'undefined') {
        event.preventDefault()

        if (typeof history.replaceState !== 'undefined') {
          history.replaceState({}, document.title, location.pathname + location.search)
        }

        element.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        })
      }
    }
  }
}
