<?php

namespace App;

/**
 * Add the theme styles to the Gravity Forms Gutenberg Block preview.
 *
 * Plugin: Gravity Forms Gutenberg Add-On
 */
add_filter('gform_preview_styles', function (array $styles, $form): array {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), get_main_style_dependencies(), null);
    wp_add_inline_style('sage/main.css', get_main_inline_styles());

    $styles[] = 'sage/main.css';

    return $styles;
}, 10, 2);
