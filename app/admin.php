<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

/*
add_action('wp_head', function () {
    if (is_user_logged_in()) {
    ?>
    <style type="text/css">
        @media (min-width: 1200px) {
            html[lang] {
                margin-top: 0 !important;
            }

            #wpadminbar {
                opacity: 0;
                transition: .05s opacity;
                transition-delay: 1s;
            }

            #wpadminbar:hover {
                opacity: 1;
                transition-delay: 0s;
            }
        }
    </style>
    <?php }
});
*/
