<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Theme fonts.
    |--------------------------------------------------------------------------
    */
    'details' => [
        'sans-serif'  => [
            'label'   => __('Sans Serif', 'sage'),
            'property'=> '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
            'source'  => ''
        ],
        'noto-serif'  => [
            'label'   => __('Noto Serif', 'sage'),
            'property'=> '"Noto Serif", serif',
            'source'  => 'https://fonts.googleapis.com/css?family=Noto+Serif'
        ],
        'heebo'  => [
            'label'   => __('Heebo', 'sage'),
            'property'=> 'Heebo, sans-serif',
            'source'  => 'https://fonts.googleapis.com/css?family=Heebo'
        ],
    ],
    'properties' => [
        'sans-serif'  => '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
        'noto-serif'  => '"Noto Serif", serif',
        'heebo'       => 'Heebo, sans-serif',
    ],
    'sources' => [
        'sans-serif'  => '',
        'noto-serif'  => 'https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i',
        'heebo'       => 'https://fonts.googleapis.com/css?family=Heebo:100,300,400,500,700,800,900',
    ],
    'labels' => [
        'sans-serif'  => __('Sans Serif', 'sage'),
        'noto-serif'  => __('Noto Serif', 'sage'),
        'heebo'       => __('Heebo', 'sage'),
    ],
    /*
    'weights' => [
        'sans-serif'  => [],
        'noto-serif'  => [400,'400i',700,'700i'],
        'heebo'       => [100,300,400,500,700,800,900],
    ],
    */
];
