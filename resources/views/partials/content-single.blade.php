@extends('layouts.article-single')

@section('entry-header')
  <div class="entry-meta-categories">
    {!!get_the_category_list(', ', null, get_the_ID())!!}
  </div>

  <h1 class="entry-title mb-1">{!! get_the_title() !!}</h1>
  @if (get_post_meta(get_the_ID(), 'lead', true))
    <p class="entry-lead lead mb-2">{{get_post_meta(get_the_ID(), 'lead', true)}}</p>
  @endif

  <div class="entry-meta mb-1">
    @include('partials/entry-meta-byline')
    @include('partials/entry-meta-time')
  </div>
@endsection

@section('entry-thumb')
  @if ('' !== get_the_post_thumbnail())
    @php(the_post_thumbnail('berggrunn-featured-image-wide', ['class' => 'img-fluid alignwide']))
  @endif
@endsection

@section('entry-content')
  @php(the_content())
@endsection

@section('entry-footer')
  {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
@endsection
