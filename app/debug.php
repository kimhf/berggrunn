<?php

function var_pump($data)
{
    if (defined('WP_ENV') && WP_ENV != 'production') {
        highlight_string("<?php\n\$data =\n" . var_export($data, true) . ";\n?>");
    }
}

function get_var_pump($data)
{
    ob_start();
    var_pump($data);
    $c = ob_get_clean();
    return $c;
}

/**
 *  Writer to the wordpress log file.
 */
if (! function_exists('write_log')) {
    function write_log($log)
    {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}

/*
add_filter('content_save_pre', function ($content) {
    $content = str_replace("http://localhost:3000", "http://localhost", $content);

    // Process content here
    return $content;
}, 10, 1);
*/

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
function sage_error($message, $subtitle = '', $title = '')
{
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};
