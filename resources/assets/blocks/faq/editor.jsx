const { registerBlockType } = wp.blocks
// const blockStyle = { backgroundColor: '#900', color: '#fff', padding: '20px' }

/**
 * @see https://gist.github.com/gordonbrander/2230317
 */
const createId = function () {
  return Math.random().toString(36).substr(2, 9)
}

registerBlockType('berggrunn/faq', {
  title: 'berggrunn/faq',

  icon: 'universal-access-alt',

  category: 'layout',

  attributes: {
    id: {
      type: 'string',
    },
    contentTitle: {
      type: 'string',
    },
    contentBody: {
      type: 'string',
    },
  },

  edit ({ attributes, className, setAttributes }) {
    const { contentTitle, contentBody, id } = attributes

    if (!id) {
      setAttributes({ id: createId() })
    }

    const onChangeContent = (e) => {
      e.preventDefault()
      setAttributes({ [e.target.name]: e.target.value })
    }

    return (
      <div className={className}>
        <input name='contentTitle' value={contentTitle} onChange={onChangeContent} type='text' />
        <textarea name='contentBody' value={contentBody} onChange={onChangeContent} />
      </div>
    )
  },

  save ({ attributes, className }) {
    // Rendering in PHP
    return null
  },

  /*
  saveQ ({ attributes, className }) {
    const { id, contentTitle, contentBody } = attributes
    return (
      <div className={className + ' cardQ'}>
        <div className='card-headerQ' id={'heading' + id}>
          <h5 className='mb-0'>
            <button className='btn btn-link collapsed' data-toggle='collapse' data-target={'#collapse' + id} aria-expanded='false' aria-controls={'collapse' + id}>{contentTitle}</button>
          </h5>
        </div>
        <div id={'collapse' + id} className='collapse' aria-labelledby={'heading' + id}>
          <div className='card-bodyQ'>{contentBody}</div>
        </div>
      </div>
    )
  },
  */
})
