<?php

namespace App;

/**
 *  Register the form elements (The widget Control)
 */
add_action('in_widget_form', function ($t, $return, $instance) {
    $instance = wp_parse_args((array) $instance, array( 'title' => '', 'text' => '', 'float' => 'none'));

    if (!isset($instance['widget_class'])) {
        $instance['widget_class'] = null;
    } ?>
    <p>
        <label for="<?php echo $t->get_field_id('widget_class'); ?>">Widget Custom Class:</label>
        <input type="text" class="widefat" name="<?php echo $t->get_field_name('widget_class'); ?>" id="<?php echo $t->get_field_id('widget_class'); ?>" value="<?php echo $instance['widget_class']; ?>" />
    </p>
    <?php
    $retrun = null;
    return array($t,$return,$instance);
}, 5, 3);

/**
 *  Save the Widget input data
 */
add_filter('widget_update_callback', function ($instance, $new_instance, $old_instance) {
    $instance['widget_class'] = strip_tags($new_instance['widget_class']);
    return $instance;
}, 5, 3);

/**
 *  Display the value in widget output
 */
add_filter('dynamic_sidebar_params', function ($params) {
    global $wp_registered_widgets;
    $widget_id = $params[0]['widget_id'];
    $widget_obj = $wp_registered_widgets[$widget_id];
    $widget_opt = get_option($widget_obj['callback'][0]->option_name);
    $widget_num = $widget_obj['params'][0]['number'];

    $widget_class = '';
    if (isset($widget_opt[$widget_num]['widget_class'])) {
        $widget_class = $widget_opt[$widget_num]['widget_class'];
    }

    /**
     * If this is a boostrap grid based widget area make sure there is a base grid css class.
     * This is important for layout as this class applies the standard grid gutters.
     */
    if (isset($params[0]['bootstrap_grid']) && $params[0]['bootstrap_grid']) {
        if (!$widget_class) {
            $widget_class = 'col';
        } else {
            preg_match("/col(?:-[1-9]{1,2}){0,1}(?=\s|$)/", $widget_class, $output_array);
            if (empty($output_array)) {
                $widget_class .= ' col';
            }
        }
    }

    if ($widget_class) {
        $params[0]['before_widget'] = preg_replace('/class="/', 'class="'.$widget_class.' ', $params[0]['before_widget'], 1);
    }

    return $params;
});
