<?php

namespace App;

/**
 *  Register our Google Api Key with the ACF Pro Map field.
 */
add_filter('acf/fields/google_map/api', function ($api) {
    if (defined('GOOGLE_MAPS_APP_KEY')) {
        $api['key'] = GOOGLE_MAPS_APP_KEY;
    }

    return $api;
});

/**
 *  Move acf json save point (folder).
 */
add_filter('acf/settings/save_json', function (string $path) : string {
    if (config('assets.acfjson')) {
        $path = config('assets.acfjson');
    }
    return $path;
}, 15, 1);

/**
 *  Add the new acf json save point (folder) to the folders to load.
 */
add_filter('acf/settings/load_json', function (array $paths) : array {
    if (config('assets.acfjson')) {
        $paths[] = config('assets.acfjson');
    }
    return $paths;
}, 5, 1);
