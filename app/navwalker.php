<?php

namespace App;

/**
 * Class Name: Bootstrap_Nav_Walker
 * Description: Based on bs4Navwalker, A custom WordPress nav walker class for Bootstrap 4 (v4.0.0-alpha.1) nav menus in a custom theme using the WordPress built in menu manager
 */

class Bootstrap_Nav_Walker extends \Walker_Nav_Menu
{
    /**
     * Starts the list before the elements are added.
     *
     * @see Walker::start_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class=\"dropdown-menu\">\n";
    }
    /**
     * Ends the list of after the elements are added.
     *
     * @see Walker::end_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</div>\n";
    }
    /**
     * Start the element output.
     *
     * @see Walker::start_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ( $depth ) ? str_repeat("\t", $depth) : '';
        $li_class_names = empty($item->classes) ? array() : (array) $item->classes;
        $li_class_names[] = 'menu-item-' . $item->ID;

        $args = (array) $args;

        $anchor_atts = array();
        $anchor_atts['title']  = ! empty($item->attr_title) ? $item->attr_title : '';
        $anchor_atts['target'] = ! empty($item->target)     ? $item->target     : '';
        $anchor_atts['rel']    = ! empty($item->xfn)        ? $item->xfn        : '';
        $anchor_atts['href']   = ! empty($item->url)        ? $item->url        : '';

        /**
         * Filter the CSS class(es) applied to a menu item's list item element.
         *
         * @since 3.0.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array  $li_class_names The CSS classes that are applied to the menu item's `<li>` element.
         * @param object $item           The current menu item.
         * @param array  $args           An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth          Depth of menu item. Used for padding.
         */
        $li_class_names = apply_filters('nav_menu_css_class', array_filter($li_class_names), $item, $args, $depth);

        /**
         * Filter the HTML attributes applied to a menu item's anchor element.
         *
         * @since 3.6.0
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
         *
         *     @type string $title  Title attribute.
         *     @type string $target Target attribute.
         *     @type string $rel    The rel attribute.
         *     @type string $href   The href attribute.
         * }
         * @param object $item  The current menu item.
         * @param array  $args  An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth Depth of menu item. Used for padding.
         */
        $anchor_atts = apply_filters('nav_menu_link_attributes', $anchor_atts, $item, $args, $depth);

        /**
         * Filter the ID applied to a menu item's list item element.
         *
         * @since 3.0.1
         * @since 4.1.0 The `$depth` parameter was added.
         *
         * @param string $menu_id The ID that is applied to the menu item's `<li>` element.
         * @param object $item    The current menu item.
         * @param array  $args    An array of {@see wp_nav_menu()} arguments.
         * @param int    $depth   Depth of menu item. Used for padding.
         */
        $id = apply_filters('nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth);

        /**
         * Add support for #hash scrolling links without reloading the page also without js.
         * Remove href address and leave #hash on links pointing to current page.
         */
        $hash = '';
        if (isset($anchor_atts['href']) && $anchor_atts['href']) {
            $href_parts = explode('#', $anchor_atts['href']);

            if (isset($href_parts[1]) && $href_parts[1] && $href_parts[0]) {
                $hash = $href_parts[1];

                $href_parts = explode('?', $href_parts[0]);

                $url = $href_parts[0];

                $query = '';
                if (isset($href_parts[1]) && $href_parts[1]) {
                    $query = $href_parts[1];
                }

                // Tries to match urls with a query string.
                // This probably needs to be improved or changed at some point.
                // We could just look for 'current-menu-item', but that does not match query strings so this seems safer for now.
                global $wp;
                $current_url = add_query_arg($_SERVER['QUERY_STRING'], '', home_url(trailingslashit($wp->request)));
                $a_url = add_query_arg($query, '', trailingslashit($url));

                if ($current_url === $a_url) {
                    $anchor_atts['href'] = '#' . $hash;
                }
            }
        }

        // Links with anchors should not have a active/current-menu-item class.
        if ($hash) {
            if (($key = array_search('current-menu-item', $li_class_names)) !== false) {
                // Remove the current-menu-item class, we do not want this item maked as active or current menu item as it will be used for anchor/scroll navigation.
                unset($li_class_names[$key]);
            }
        }

        /**
         * Create a string used for the menu item's list item element class attr.
         */
        $li_class_names[] = 'nav-item';

        if (in_array('menu-item-has-children', $li_class_names)) {
            $li_class_names[] = 'dropdown';
        }

        if (in_array('current-menu-item', $li_class_names)) {
            $li_class_names[] = 'active';
        }

        /**
         * Create a string used for the menu item's list item id attr
         */
        $li_id = $id ? ' id="' . esc_attr($id) . '"' : '';

        /**
         * Add the menu item's list item output
         */
        if ($depth === 0) {
            $output .= $indent . '<li' . $li_id . ' class="' . esc_attr(implode(' ', $li_class_names)) . '">';
        }

        /**
         * Add thme/bootstrap specific attributes to a menu item's anchor element.
         */
        if ($depth === 0) {
            $anchor_atts['class'] = 'nav-link';
        }
        if ($depth === 0 && in_array('menu-item-has-children', $li_class_names)) {
            $anchor_atts['class']       .= ' dropdown-toggle';
            $anchor_atts['data-toggle']  = 'dropdown';
        }
        if ($depth > 0) {
            $manual_class = array_values($li_class_names)[0] .' '. 'dropdown-item';
            $anchor_atts['class']= $manual_class;
        }

        if (in_array('current-menu-item', $li_class_names)) {
            $anchor_atts['class'] .= ' active';
        }


        $anchor_attributes = '';
        foreach ($anchor_atts as $attr => $value) {
            if (! empty($value)) {
                $value = ( 'href' === $attr ) ? esc_url($value) : esc_attr($value);
                $anchor_attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args['before'];
        $item_output .= '<a'. $anchor_attributes .'>';

        /** This filter is documented in wp-includes/post-template.php */
        $item_output .= $args['link_before'] . apply_filters('the_title', $item->title, $item->ID) . $args['link_after'];
        $item_output .= '</a>';
        $item_output .= $args['after'];

        /**
         * Filter a menu item's starting output.
         *
         * The menu item's starting output only includes `$args->before`, the opening `<a>`,
         * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
         * no filter for modifying the opening and closing `<li>` for a menu item.
         *
         * @since 3.0.0
         *
         * @param string $item_output The menu item's starting HTML output.
         * @param object $item        Menu item data object.
         * @param int    $depth       Depth of menu item. Used for padding.
         * @param array  $args        An array of {@see wp_nav_menu()} arguments.
         */
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
    /**
     * Ends the element output, if needed.
     *
     * @see Walker::end_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Page data object. Not used.
     * @param int    $depth  Depth of page. Not Used.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        if ($depth === 0) {
            $output .= "</li>\n";
        }
    }
}
