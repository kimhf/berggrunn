<?php

namespace App;

function get_excerpt($length = 0)
{
    if ($length) {
        Excerpt::get_instance()::set_length($length);
    }

    $excerpt = get_the_excerpt();

    // Remove &nbsp; from content, should be filteres out in content_save_pre filter for future posts.
    $nbsp = html_entity_decode("&nbsp;");
    $excerpt = html_entity_decode($excerpt);
    $excerpt = str_replace($nbsp, " ", $excerpt);
    $excerpt = preg_replace('/\s+/', ' ', $excerpt);

    $excerpt = wpautop(wp_trim_words($excerpt, $length, " [...]"));

    if ($length) {
        Excerpt::get_instance()::reset_length();
    }

    return $excerpt;
}

/**
 * Returns the navigation to next/previous set of posts, when applicable.
 *
 * @param array $args {
 *     Optional. Default posts navigation arguments. Default empty array.
 *
 *     @type string $prev_text          Anchor text to display in the previous posts link.
 *                                      Default 'Older posts'.
 *     @type string $next_text          Anchor text to display in the next posts link.
 *                                      Default 'Newer posts'.
 *     @type string $screen_reader_text Screen reader text for nav element.
 *                                      Default 'Posts navigation'.
 * }
 * @return string Theme specific markup for posts links.
 */
function get_the_posts_navigation(array $args = []) : string {
    $nav = \get_the_posts_navigation($args);

    $nav = str_replace('nav-links', 'nav-links clearfix', $nav);
    $nav = str_replace('<a', '<a class="btn btn-dark"', $nav);
    $nav = str_replace('nav-previous', 'nav-previous float-left', $nav);
    $nav = str_replace('nav-next', 'nav-next float-right', $nav);

    return $nav;
}
