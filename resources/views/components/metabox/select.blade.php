<div class="components-base-control editor-page-attributes__template">
  <div class="components-base-control__field">
  <label class="components-base-control__label" for="{{$name}}">{{$label}}</label>
    <select class="components-select-control__input" name="{{$name}}">
      @foreach($choices as $key => $choice)
      @php
      $select = ($value == $key) ? ' selected' : '';
      @endphp
      <option value="{{$key}}"{{$select}}>{{$choice}}</option>
      @endforeach
    </select>
    @if (isset($description) && $description)
    <p class="components-base-control__help">
      {{$description}}
    </p>
    @endif
  </div>
</div>
