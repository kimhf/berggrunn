<?php

namespace App;

use Roots\Sage\Container;

/**
 * Get the sage container.
 *
 * @param string $abstract
 * @param array  $parameters
 * @param Container $container
 * @return Container|mixed
 */
function sage($abstract = null, $parameters = [], Container $container = null)
{
    $container = $container ?: Container::getInstance();
    if (!$abstract) {
        return $container;
    }
    return $container->bound($abstract)
        ? $container->makeWith($abstract, $parameters)
        : $container->makeWith("sage.{$abstract}", $parameters);
}

/**
 * Get / set the specified configuration value.
 *
 * If an array is passed as the key, we will assume you want to set an array of values.
 *
 * @param array|string $key
 * @param mixed $default
 * @return mixed|\Roots\Sage\Config
 * @copyright Taylor Otwell
 * @link https://github.com/laravel/framework/blob/c0970285/src/Illuminate/Foundation/helpers.php#L254-L265
 */
function config($key = null, $default = null)
{
    if (is_null($key)) {
        return sage('config');
    }
    if (is_array($key)) {
        return sage('config')->set($key);
    }
    return sage('config')->get($key, $default);
}

/**
 * @param string $file
 * @param array $data
 * @return string
 */
function template($file, $data = [])
{
    if (!is_admin() && remove_action('wp_head', 'wp_enqueue_scripts', 1)) {
        wp_enqueue_scripts();
    }

    return sage('blade')->render($file, $data);
}

/**
 * Retrieve path to a compiled blade view
 * @param $file
 * @param array $data
 * @return string
 */
function template_path($file, $data = [])
{
    return sage('blade')->compiledPath($file, $data);
}

/**
 * @param $asset
 * @return string
 */
function asset_path($asset)
{
    return sage('assets')->getUri($asset);
}

/**
 * @param $asset
 * @return string
 */
function asset_dir($asset):string
{
    return config('assets.dir') . '/' . sage('assets')->get($asset);
}

/**
 * @param string|string[] $templates Possible template files
 * @return array
 */
function filter_templates($templates)
{
    $paths = apply_filters('sage/filter_templates/paths', [
        'views',
        'resources/views'
    ]);
    $paths_pattern = "#^(" . implode('|', $paths) . ")/#";

    return collect($templates)
        ->map(function ($template) use ($paths_pattern) {
            /** Remove .blade.php/.blade/.php from template names */
            $template = preg_replace('#\.(blade\.?)?(php)?$#', '', ltrim($template));

            /** Remove partial $paths from the beginning of template names */
            if (strpos($template, '/')) {
                $template = preg_replace($paths_pattern, '', $template);
            }

            return $template;
        })
        ->flatMap(function ($template) use ($paths) {
            return collect($paths)
                ->flatMap(function ($path) use ($template) {
                    return [
                        "{$path}/{$template}.blade.php",
                        "{$path}/{$template}.php",
                        "{$template}.blade.php",
                        "{$template}.php",
                    ];
                });
        })
        ->filter()
        ->unique()
        ->all();
}

/**
 * @param string|string[] $templates Relative path to possible template files
 * @return string Location of the template
 */
function locate_template($templates)
{
    return \locate_template(filter_templates($templates));
}

/**
 * Determine whether to show the sidebar
 * @return bool
 */
function display_sidebar()
{
    static $display;
    isset($display) || $display = apply_filters('sage/display_sidebar', false);
    return $display;
}

/**
 * Returns the theme options for a front end context.
 * Defaults, customizer choices and post meta choices will be merged together to a array.
 *
 * @return array
 */
function get_contextual_theme_mods()
{
    static $theme_mods;

    if ($theme_mods) {
        return $theme_mods;
    }

    if (is_singular()) {
        $post_theme_options = get_post_meta(get_the_ID(), 'berggrunn', true);
        if (is_array($post_theme_options) && !empty($post_theme_options)) {
            $theme_mods = $post_theme_options;
        }
    }

    $theme_options = config('theme_options');
    $customizer_theme_mods = [];
    foreach ($theme_options as $theme_option_name => $theme_option) {
        $theme_mod = get_theme_mod($theme_option_name, $theme_option['default']);

        $customizer_theme_mods[$theme_option_name] = $theme_mod;
    }

    // $customizer_theme_options = get_theme_mod('berggrunn', false);
    if (is_array($customizer_theme_mods) && !empty($customizer_theme_mods)) {
        $theme_mods = wp_parse_args($theme_mods, $customizer_theme_mods);
    }

    // Apply defaults for missing keys.
    $theme_default_options = [
        'navbar__placement'       => '',
        'navbar__inner_container' => '',
        'page_header__visability'  => ''
    ];

    $theme_mods = wp_parse_args($theme_mods, $theme_default_options);

    return $theme_mods;
}

function get_contextual_theme_mod(string $mod)
{
    $theme_mods = get_contextual_theme_mods();

    if (isset($theme_mods[$mod])) {
        return $theme_mods[$mod];
    }

    return '';
}

/**
 * Function to apply attributes to HTML tags.
 * Devs can override attributes in a child theme by using the correct slug.
 *
 * @param  string $slug    Slug to refer to the HTML tag.
 * @param  array  $attr    Attributes for HTML tag.
 * @return [type]            [description]
 */
function attributes(string $slug, array $attr = []):string
{
    if (empty($attr)) {
        $attr['class'] = $slug;
    }

    return html_attributes($attr);
}

/**
 * Function to transform an array of attributes to a string with HTML attributes.
 *
 * @param  string $slug    Slug to refer to the HTML tag.
 * @param  array  $attributes Attributes for HTML tag.
 * @return [type]            [description]
 */
function html_attributes(array $attr):string
{
    $out = '';

    foreach ($attr as $name => $value) {
        if (is_array($value)) {
            $value = implode(' ', array_filter($value));
        }
        $out .= (! empty($value) || strlen($value) > 0 || is_bool($value)) ? ' ' . esc_html($name) . '="' . esc_attr($value) . '"' : '';
    }

    return trim($out);
}

/**
 * Function to generate a scoped style element.
 *
 * @param  string $scope_id    The Unique id to the parrent element, used to support browers without support for scoped styles.
 *                             or when styles are positioned in <head>
 * @param  array  $sheet       The stylesheet in array form.
 * @return string              The style html tag
 */
function get_scoped_styles($scope_id, $sheet, $scoped = false)
{
    $html = '';

    $html = get_styles($sheet, $scope_id);

    if ($html) {
        if ($scoped) {
            $html = '<style scoped="scoped">' . $html . '</style>';
        } else {
            $html = '<style>' . $html . '</style>';
        }
    }
    return $html;
}

/**
 * Function to transform an array of styles into css styles.
 *
 * @param  string $scope_id    The Unique id to the parrent element, used to support browers without support for scoped styles.
 * @param  array  $sheet       The stylesheet in array form.
 * @return string              The style html tag
 */
function get_styles($sheet, $scope_id = '')
{
    $html = '';

    if ($sheet) {
        foreach ($sheet as $media_query => $styles) {
            if ($media_query) {
                $html .= $media_query . '{';
            }
            foreach ($styles as $selector => $style) {
                $sep = ' ';
                if (strpos($selector, ':') === 0) {
                    $sep = '';
                }

                if (! is_array($style)) {
                    $style = [$style];
                }

                if ($scope_id) {
                    $html .= '#' . $scope_id . $sep;
                }

                $html .= $selector . '{';

                foreach ($style as $s) {
                    $html .= $s;
                }
                $html .= '}';
            }
            if ($media_query) {
                $html .= '}';
            }
        }
    }

    return $html;
}

/**
 * Remove HTML elements with certain classnames (or IDs) from HTML string
 * Based on BJLLs remove_skip_classes_elements()
 *
 * @param string $content       The HTML string
 * @param string $skip_classes  The css classes for the elements to remove
 * @return string               The HTML string without the unwanted elements
 */
/*
function remove_classes_elements(string $content, array $skip_classes):string {
    // Parsing html with regex is not a good solution, but it often works.
    $skip_classes_quoted = array_map( 'preg_quote', $skip_classes );
    $skip_classes_ORed = implode( '|', $skip_classes_quoted );

    $regex = '/<\s*\w*\s*class\s*=\s*[\'"](|.*\s)' . $skip_classes_ORed . '(|\s.*)[\'"].*>/isU';

    return preg_replace( $regex, '', $content );
}
*/

/**
 * Get the aspect ratio from a html tag with height and width attributes.
 *
 * @param string $html
 * @return string
 */
function get_element_aspectratio(string $html) : float
{
    $shortcodeish_html = str_replace(['<', '>'], ['[', ']'], $html);
    $shortcodeish_atts = shortcode_parse_atts($shortcodeish_html);

    $ratio = 56.25;
    if (isset($shortcodeish_atts['width']) && isset($shortcodeish_atts['height'])) {
        $ratio = ($shortcodeish_atts['height'] / ($shortcodeish_atts['width'])) * 100;
        $ratio = number_format($ratio, 3, '.', '');
    }
    return $ratio;
}

/**
 * Add wrappers to images in the provided content.
 * This will preserve aspect ratio during lazyloading, and prevent content jumping.
 *
 * @param string $content
 * @return string
 */
function aspectratio_wrap_images(string $content) : string {
    $matches = [];
    preg_match_all( '/<img[\s\r\n]+.*?>/is', $content, $matches );

    $search = [];
    $replace = [];

    foreach ($matches[0] as $imgHTML) {
        // Don't do anything if the image is a data-uri
        if ( preg_match( "/src=['\"]data:image/is", $imgHTML ) ) {
            continue;
        }

        // The wrapper must be applied before the resource is made lazy.
        if ( strpos( $imgHTML, 'lazy' ) ) {
            continue;
        }

        $sheet = [];

        $ratio = get_element_aspectratio($imgHTML);

        $replaceHTML = get_element_aspectratio_wrapped($imgHTML, $ratio, ['class' => 'image-wrapper']);

        array_push( $search, $imgHTML );
        array_push( $replace, $replaceHTML );
    }

    $content = str_replace( $search, $replace, $content );

    return $content;
}

/**
 * Add wrappers to iframes in the provided content.
 * This will preserve aspect ratio during lazyloading, and prevent content jumping.
 *
 * @param string $content
 * @return string
 */
function aspectratio_wrap_iframes(string $content) : string {
    $scope_id = uniqid('scope-');

    $matches = [];
    preg_match_all( '|<iframe\s+.*?</iframe>|si', $content, $matches );

    $search = [];
    $replace = [];

    foreach ($matches[0] as $iframeHTML) {
        // Don't mess with the Gravity Forms ajax iframe
        if ( strpos( $iframeHTML, 'gform_ajax_frame' ) ) {
            continue;
        }

        // The wrapper must be applied before the resource is made lazy.
        if ( strpos( $iframeHTML, 'lazy' ) ) {
            continue;
        }

        $sheet = [];

        $ratio = get_element_aspectratio($iframeHTML);

        $replaceHTML = get_element_aspectratio_wrapped($iframeHTML, $ratio, ['class' => 'iframe-wrapper']);

        array_push( $search, $iframeHTML );
        array_push( $replace, $replaceHTML );
    }

    $content = str_replace( $search, $replace, $content );

    return $content;
}

/**
 * Add a wrapper around element to preserve its aspect ratio while lazy loading.
 */
function get_element_aspectratio_wrapped(string $html, $ratio, array $args = []) {
    $args = wp_parse_args($args, [
        'class' => '',
        'scope_id' => uniqid('scope-')
    ]);

    $wrapper_attr = [];
    $wrapper_attr['id'] = $args['scope_id'];
    $wrapper_attr['class'][] = 'aspect-wrapper';

    if ($args['class']) {
        $wrapper_attr['class'][] = $args['class'];
    }

    if ($ratio) {
        $sheet['']['::before'] = 'padding-bottom:' . $ratio . '%;';
        $wrapper_attr['class'][] = 'preserve-aspect-ratio';
    }

    return '<span ' . html_attributes($wrapper_attr) . '>' . get_scoped_styles($args['scope_id'], $sheet, true) . $html . '</span>';
}
