<?php

namespace App;

/**
 * Required widget files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
add_action('widgets_init', function () {
    array_map(function ($widget) {
        $file = __DIR__ . "/class-{$widget}.php";
        if (!file_exists($file)) {
            sage_error(sprintf(__('Error locating widget <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
        } else {
            require_once($file);
            register_widget(__NAMESPACE__ . '\\' . $widget);
        }
    }, [
        'Custom_Logo'
    ]);
});
