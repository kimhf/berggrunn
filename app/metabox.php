<?php

namespace App;

add_action('add_meta_boxes', function () {
    add_meta_box(
        'berggrunn-navbar',
        'Navbar',
        __NAMESPACE__ . '\\navbar_meta_box_callback',
        ['post', 'page'],
        'side',
        'low',
        array(
            '__block_editor_compatible_meta_box' => true,
        )
    );

    add_meta_box(
        'berggrunn-title',
        'Title',
        __NAMESPACE__ . '\\title_meta_box_callback',
        ['page'],
        'side',
        'low',
        array(
            '__block_editor_compatible_meta_box' => true,
        )
    );
});

function navbar_meta_box_callback()
{
    global $post;
    $post_id = $post->ID;

    $berggrunn_post_meta = get_post_meta($post_id, 'berggrunn', true);

    $value = isset($berggrunn_post_meta['navbar__placement']) ? $berggrunn_post_meta['navbar__placement'] : '';
    echo template('components.metabox.select', wp_parse_args([
        'name'  => 'berggrunn[navbar__placement]',
        'value' => $value,
    ], config('theme_options.navbar__placement')));

    $value = isset($berggrunn_post_meta['navbar__inner_container']) ? $berggrunn_post_meta['navbar__inner_container'] : '';
    echo template('components.metabox.select', wp_parse_args([
        'name'  => 'berggrunn[navbar__inner_container]',
        'value' => $value,
    ], config('theme_options.navbar__inner_container')));

    $value = isset($berggrunn_post_meta['navbar__anchor']) ? $berggrunn_post_meta['navbar__anchor'] : '';
    echo template('components.metabox.textInput', [
        'label' => 'HTML Anchor',
        'description' => __('Anchors lets you link directly to a section on a page. This anchor will point to the top of the page.'),
        'name'  => 'berggrunn[navbar__anchor]',
        'value' => $value,
    ]);
}

function title_meta_box_callback()
{
    global $post;
    $post_id = $post->ID;

    $berggrunn_post_meta = get_post_meta($post_id, 'berggrunn', true);

    $value = isset($berggrunn_post_meta['page_header__visability']) ? $berggrunn_post_meta['page_header__visability'] : '';
    echo template('components.metabox.select', [
        'name'  => 'berggrunn[page_header__visability]',
        'value' => $value,
        'label' => 'Title visability',
        'choices' => [
            ''       => __('Visible', 'sage'),
            'hidden' => __('Hidden', 'sage'),
        ],
    ]);
}

function save_navbar_meta_box_callback($post_id)
{

    /*
    if (! isset($_POST['nonce'])) {
        return;
    }

    if (! wp_verify_nonce($_POST['nonce'], 'nonce_value')) {
        return;
    }
    */
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        write_log('DOING_AUTOSAVE');

        return;
    }

    if (! current_user_can('edit_post', $post_id)) {
        return;
    }

    if (isset($_POST['post_type']) && 'page' === $_POST['post_type'] && isset($_POST['berggrunn']) && $_POST['berggrunn']) {
        $post_options = $_POST['berggrunn'];
        $options = [];

        if (isset($post_options['navbar__anchor']) && $post_options['navbar__anchor']) {
            $options['navbar__anchor'] = esc_attr($post_options['navbar__anchor']);
        }

        if (isset($post_options['navbar__placement']) && $post_options['navbar__placement']) {
            $options['navbar__placement'] = esc_attr($post_options['navbar__placement']);
        }

        if (isset($post_options['navbar__inner_container']) && $post_options['navbar__inner_container']) {
            $options['navbar__inner_container'] = esc_attr($post_options['navbar__inner_container']);
        }

        if (isset($post_options['page_header__visability']) && $post_options['page_header__visability']) {
            $options['page_header__visability'] = esc_attr($post_options['page_header__visability']);
        }

        update_post_meta($post_id, 'berggrunn', $options);
    }

    return $post_id;
}
add_action('save_post', __NAMESPACE__ . '\\save_navbar_meta_box_callback');
