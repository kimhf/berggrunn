@extends('layouts.app')

@section('content')
  <div class="container">
    @include('partials.page-header')

    @if (!have_posts())
      <div class="alert alert-warning">
        {{ __('Sorry, no results were found.', 'sage') }}
      </div>
      {!! get_search_form(false) !!}
    @endif

    <div class="row">
    @while (have_posts()) @php(the_post())
      <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-r">
      @include('partials.content-'.get_post_type())
      </div>
    @endwhile
    </div>

    <div class="mb-r">
      {!! App\get_the_posts_navigation() !!}
    </div>
  </div>
@endsection
