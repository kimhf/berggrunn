/* global Modernizr */

/**
 * Usage:
 *
 * const isInQuery = mediaQuery('(max-width: 600px)');
 */
export default function mediaQuery (mediaQuery) {
  if (typeof (Modernizr) !== 'undefined' && typeof (Modernizr.mq) !== 'undefined') {
    if (Modernizr.mq(mediaQuery)) {
      return true
    }
  } else if (typeof (window.matchMedia) !== 'undefined') {
    if (window.matchMedia(mediaQuery).matches) {
      return true
    }
  }
  return false
}
