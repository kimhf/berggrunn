<?php

namespace App;

/**
 * Required plugin-compat files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($compat) {
    $file = __DIR__ . "/{$compat}.php";
    if (!file_exists($file)) {
        sage_error(sprintf(__('Error locating plugin-compat <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    } else {
        require_once($file);
    }
}, [
    'gravityforms-gutenberg-addon',
    'gravityforms',
    'acf_pro'
]);
