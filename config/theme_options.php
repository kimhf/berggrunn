<?php

return [
    'navbar__placement' => [
        'type'    => 'select',
        'label'   => __('Placement', 'sage'),
        'section' => 'navbar',
        'choices' => [
            ''             => __('Default', 'sage'),
            'absolute-top' => __('Absolute', 'sage'),
            'fixed-top'    => __('Fixed', 'sage'),
            'sticky-top'   => __('Sticky', 'sage')
        ],
        'default' => ''
    ],
    'navbar__inner_container' => [
        'type'    => 'select',
        'label'   => __('Inner Container', 'sage'),
        'section' => 'navbar',
        'choices' => [
            ''                => __('Default', 'sage'),
            'none'            => __('None', 'sage'),
            'container'       => __('Container', 'sage'),
            'container-fluid' => __('Fluid', 'sage')
        ],
        'default' => ''
    ],
    'navbar__color' => [
        'type'    => 'select',
        'label'   => __('Text Color', 'sage'),
        'section' => 'navbar',
        'choices' => [
            'navbar-light'       => __('Dark', 'sage'),
            'navbar-dark' => __('Light', 'sage'),
            '' => __('None', 'sage')
        ],
        'default' => 'navbar-light'
    ],
    'navbar__background' => [
        'type'    => 'select',
        'label'   => __('Background', 'sage'),
        'section' => 'navbar',
        'choices' => [
            'bg-white'       => __('White', 'sage'),
            'bg-dark'       => __('Dark', 'sage'),
            'bg-primary' => __('Primary', 'sage'),
            '' => __('None', 'sage')
        ],
        'default' => 'bg-white'
    ],
    'footer__class' => [
        'type'    => 'text',
        'label' => __('Additional CSS Class', 'sage'),
        'section' => 'footer',
        'default' => ''
    ],
    'font_family_base' => [
        'type'    => 'select',
        'label'   => __('Font Family Base', 'sage'),
        'section' => 'typography',
        'choices' => [],
        'default' => 'sans-serif',
    ],
    'font_family_headings' => [
        'type'    => 'select',
        'label'   => __('Font Family Headings', 'sage'),
        'section' => 'typography',
        'choices' => [],
        'default' => 'noto-serif',
    ],
];
