<?php

namespace App;

// Move customizer styles to the end of the document to take high priority.
// remove_action('wp_head', 'wp_custom_css_cb', 101);
// add_action('wp_footer', 'wp_custom_css_cb', PHP_INT_MAX);

function get_main_style_dependencies()
{
    $main_deps = [];

    $font_family_base = get_theme_mod('font_family_base', config('styles.font_family_base'));

    if (isset(config('fonts.labels')[$font_family_base]) && config('fonts.sources')[$font_family_base]) {
        $font_family_base_name = 'font/' . config('fonts.labels')[$font_family_base];
        wp_register_style($font_family_base_name, config('fonts.sources')[$font_family_base], false, null);
        $main_deps[] = $font_family_base_name;
    }

    $font_family_headings = get_theme_mod('font_family_headings', config('styles.font_family_headings'));

    if (isset(config('fonts.labels')[$font_family_headings]) && config('fonts.sources')[$font_family_headings]) {
        $font_family_headings_name = 'font/' . config('fonts.labels')[$font_family_headings];
        wp_register_style($font_family_headings_name, config('fonts.sources')[$font_family_headings], false, null);
        $main_deps[] = $font_family_headings_name;
    }

    return $main_deps;
}

function get_main_inline_styles()
{
    static $inline_styles;

    if ($inline_styles) {
        return $inline_styles;
    }

    $custom_vars = [];

    $font_family_base = get_theme_mod('font_family_base', config('styles.font_family_base'));
    if (isset(config('fonts.properties')[$font_family_base])) {
        $custom_vars[] = '--font-family-base: ' . config('fonts.properties')[$font_family_base] . ';';
    }

    $font_family_headings = get_theme_mod('font_family_headings', config('styles.font_family_headings'));
    if (isset(config('fonts.properties')[$font_family_headings])) {
        $custom_vars[] = '--font-family-headings: ' . config('fonts.properties')[$font_family_headings] . ';';
    }

    // Setup the primary color
    $primary_color = get_theme_mod('primary_color', config('styles.primary_color'));
    $custom_vars = array_merge($custom_vars, create_theme_color_variant('primary', $primary_color));

    // Setup the secondary color
    $secondary_color = get_theme_mod('secondary_color', config('styles.secondary_color'));
    $custom_vars = array_merge($custom_vars, create_theme_color_variant('secondary', $secondary_color));

    $inline_styles = ":root{" . implode('', $custom_vars) . "}";

    return $inline_styles;
}

function create_theme_color_variant(string $name, string $hex): array
{
    $vars = [];
    $Color = new \Mexitek\PHPColors\Color($hex);

    $vars[] = "--$name-darken-12_5: #{$Color->darken(12.5)};";
    $vars[] = "--$name-darken-10: #{$Color->darken(10)};";
    $vars[] = "--$name-darken-7_5: #{$Color->darken(7.5)};";
    $vars[] = "--$name-darken-5: #{$Color->darken(5)};";
    $vars[] = "--$name-darken-2_5: #{$Color->darken(2.5)};";

    $vars[] = "--$name: {$hex};";

    $vars[] = "--$name-lighten-2_5: #{$Color->lighten(2.5)};";
    $vars[] = "--$name-lighten-5: #{$Color->lighten(5)};";
    $vars[] = "--$name-lighten-7_5: #{$Color->lighten(7.5)};";
    $vars[] = "--$name-lighten-10: #{$Color->lighten(10)};";
    $vars[] = "--$name-lighten-12_5: #{$Color->lighten(12.5)};";

    $color_rgb = $Color->getRgb();
    $vars[] = "--$name-r: {$color_rgb['R']};";
    $vars[] = "--$name-g: {$color_rgb['G']};";
    $vars[] = "--$name-b: {$color_rgb['B']};";

    return $vars;
}
