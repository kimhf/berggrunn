const hotMiddlewareScript = require(`webpack-hot-middleware/client?autoConnect=false`)

hotMiddlewareScript.setOptionsAndConnect({
  noInfo: true,
  timeout: 20000,
  reload: true,
  overlayStyles: JSON.stringify({
    zIndex: 99999,
  }),
})

hotMiddlewareScript.subscribe(event => {
  if (event.action === 'reload') {
    window.location.reload()
  }
})
