import { supportsCSSVariables, detectIE } from './tests'

if (process.env.NODE_ENV === 'production') {
  /**
   * Lazy load css variables Polyfill
   */
  if (!supportsCSSVariables()) {
    import('./cssVarsPonyfill').then((module) => {
      module.polyfill()
    }).catch((e) => {
      console.log(e)
    })
  }

  /**
   * Lazy load the smoothscroll Polyfill.
   *
   * This is low priority as it only ensures a smooth scroll effect instead of simply jumping to the anchor.
   *
   * There are no accurate tests for this, but testing for ie should cover a lot of the cases.
   */
  if (detectIE() || typeof document.body.scrollIntoView as any === 'undefined') {
    import('smoothscroll-polyfill').then((module) => {
      module.polyfill()
    }).catch((e) => {
      console.log(e)
    })
  }
}
