@extends('layouts.article-single')

@if ($show_page_header)
  @section('entry-header')
    <h1 class="entry-title mb-1">{!! get_the_title() !!}</h1>
  @endsection
@endif

@section('entry-content')
  @php(the_content())
@endsection

@section('entry-footer')
  {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
@endsection
