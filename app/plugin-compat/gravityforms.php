<?php

namespace App;

/**
 * Changes the default Gravity Forms AJAX spinner.
 */
add_filter('gform_ajax_spinner_url', function ($image_src, $form) {
    return asset_path('images/loader.png');
}, 10, 2);

/**
 * Remove default gravityforms editor styles.
 */
remove_filter('tiny_mce_before_init', ['GFForms', 'modify_tiny_mce_4'], 20);

/**
 * Remove Gravity forms styles
 */
add_action('gform_enqueue_scripts', function () {
    global $wp_styles;
    if (isset($wp_styles->registered['gforms_datepicker_css'])) {
        unset($wp_styles->registered['gforms_datepicker_css']);
    }
    if (isset($wp_styles->registered['gforms_formsmain_css'])) {
        unset($wp_styles->registered['gforms_formsmain_css']);
    }
    if (isset($wp_styles->registered['gforms_ready_class_css'])) {
        unset($wp_styles->registered['gforms_ready_class_css']);
    }
    if (isset($wp_styles->registered['gforms_browsers_css'])) {
        unset($wp_styles->registered['gforms_browsers_css']);
    }
});
