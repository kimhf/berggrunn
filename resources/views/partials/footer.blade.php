<footer class="content-info{{$footer_class}}">
  <div class="container">
    @if(is_active_sidebar( 'sidebar-footer' ))
    <div class="row sidebar-footer">
      @php(dynamic_sidebar('sidebar-footer'))
    </div>
    @endif
  </div>
</footer>
