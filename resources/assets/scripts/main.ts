/* global jQuery */

// Import babel polyfill
// import 'babel-polyfill'
import 'core-js/es6/promise'
import 'core-js/es6/object'
import './pollyfills'

// console.log('typeof test', typeof test)

// import external dependencies
// import { jQuery } from 'jquery'

// Import everything from autoload
// import './autoload/**/*'


// import local dependencies
import Router from './util/Router'
import common from './routes/common'
import home from './routes/home'
import aboutUs from './routes/about'
import textareaAutoExpand from './lib/textareaAutoExpand'

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
})

// Load Events
jQuery(() => routes.loadEvents())

// Initialize autoexpanding textareas.
textareaAutoExpand()



// if (!hasNativeCSSProperties()) {
// @ts-ignore
// require.ensure('css-vars-ponyfill', (require) => {

// eslint ignore



// console.log('css-vars-ponyfill')

// })
// }
