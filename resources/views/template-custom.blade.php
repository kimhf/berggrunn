{{--
  Template Name: Custom Template
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    @if ($show_page_header)
      @include('partials.page-header')
    @endif
    @include('partials.content-page')
  @endwhile
@endsection
