<?php

namespace App;

function wp_head()
{
    $theme_mods = get_theme_mods();

    if (isset($theme_mods['fonts']) && $theme_mods['fonts']) {
        echo $theme_mods['fonts'];
    }
    if (isset($theme_mods['tracking']) && $theme_mods['tracking']) {
        echo $theme_mods['tracking'];
    }
    if (isset($theme_mods['google_analytics']) && $theme_mods['google_analytics']) {
        echo $theme_mods['google_analytics'];
    }
    if (isset($theme_mods['facebook_pixel']) && $theme_mods['facebook_pixel']) {
        echo $theme_mods['facebook_pixel'];
    }
}
add_action('wp_head', __NAMESPACE__ . '\\wp_head', 9);
