<article @php(post_class('entry-card'))>
  <a href="{{ get_permalink() }}">
    <div class="entry-thumb mb-2">
    @if ('' !== get_the_post_thumbnail())
      @php(the_post_thumbnail('berggrunn-featured-image-wide', ['class' => 'img-fluid']))
    @endif
    </div>
    <header>
      <h2 class="entry-title">{!! get_the_title() !!}</h2>
    </header>
    <div class="entry-summary small">
      {!!App\get_excerpt(20)!!}
    </div>
    <div class="entry-meta small">
      <span class="entry-meta-item byline author vcard">
        {{ __('By', 'sage') }} {{ get_the_author() }}
      </span>
    </div>
  </a>
</article>
