<article @php(post_class('entry-article'))>
  @hasSection('entry-header')
    <header class="entry-header alignment-ready">
      @yield('entry-header')
    </header>
  @endif
  @hasSection('entry-thumb')
    <div class="entry-thumb alignment-ready mb-r">
      @yield('entry-thumb')
    </div>
  @endif
  @hasSection('entry-content')
    <div class="entry-content alignment-ready">
      @yield('entry-content')
    </div>
  @endif
  @hasSection('entry-footer')
    <footer class="entry-footer alignment-ready">
      @yield('entry-footer')
    </footer>
  @endif
  <div class="entry-comments alignment-ready">
    @php(comments_template('/partials/comments.blade.php'))
  </div>
</article>
