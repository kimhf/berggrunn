<div class="components-base-control editor-page-attributes__template">
  <div class="components-base-control__field">
  <label class="components-base-control__label" for="{{$name}}">{{$label}}</label>
  <input type="text" class="components-text-control__input" name="{{$name}}" value="{{$value}}"/>
  @if (isset($description) && $description)
  <p class="components-base-control__help">
    {{$description}}
  </p>
  @endif
  </div>
</div>
