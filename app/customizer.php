<?php

namespace App;

function customize_register($wp_customize)
{

    //  =============================
    //  = Colors                    =
    //  =============================
    $wp_customize->add_section('colors', [
        'title'    => __('Colors', 'sage'),
        'description' => '',
        'priority' => 110,
    ]);

    // Primary color
    $wp_customize->add_setting('primary_color', [
        'default'   => config('styles.primary_color'),
        'transport' => 'refresh',
    ]);

    $wp_customize->add_control(new \WP_Customize_Color_Control($wp_customize, 'primary_color', [
        'section' => 'colors',
        'label'   => esc_html__('Primary color', 'sage'),
    ]));

    // Secondary color
    $wp_customize->add_setting('secondary_color', [
        'default'   => config('styles.secondary_color'),
        'transport' => 'refresh',
    ]);

    $wp_customize->add_control(new \WP_Customize_Color_Control($wp_customize, 'secondary_color', [
        'section' => 'colors',
        'label'   => esc_html__('Secondary color', 'sage'),
    ]));

    //  =============================
    //  = Typography                =
    //  =============================
    $wp_customize->add_section('typography', [
        'title'    => __('Typography', 'sage'),
        'description' => '',
        'priority' => 111,
    ]);

    //  =============================
    //  = Select Input -            =
    //  =============================
    $wp_customize->add_setting('font_family_base', [
        'default'        => config('theme_options.font_family_base.default'),
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ]);

    $wp_customize->add_control('font_family_base', [
        'type'        => 'select',
        'label'       => config('theme_options.font_family_base.label'),
        // 'description' => __(''),
        'section'     => config('theme_options.font_family_base.section'),
        'settings'    => 'font_family_base',
        'choices'     => config('fonts.labels'),
    ]);

    //  =============================
    //  = Select Input -            =
    //  =============================
    $wp_customize->add_setting('font_family_headings', [
        'default'        => config('theme_options.font_family_base.default'),
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ]);

    $wp_customize->add_control('font_family_headings', [
        'type'        => 'select',
        'label'       => config('theme_options.font_family_headings.label'),
        // 'description' => __(''),
        'section'     => config('theme_options.font_family_headings.section'),
        'settings'    => 'font_family_headings',
        'choices'     => config('fonts.labels'),
    ]);

    //  =============================
    //  = Navbar                    =
    //  =============================
    $wp_customize->add_section('navbar', [
        'title'    => __('Navbar', 'sage'),
        'description' => '',
        'priority' => 120,
    ]);

    //  =============================
    //  = Select Input -            =
    //  =============================
    $wp_customize->add_setting('navbar__placement', [
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ]);

    $wp_customize->add_control('navbar__placement', [
        'type'        => 'select',
        'label'       => config('theme_options.navbar__placement.label'),
        // 'description' => __(''),
        'section'     => config('theme_options.navbar__placement.section'),
        'settings'    => 'navbar__placement',
        'choices'     => config('theme_options.navbar__placement.choices'),
    ]);

    //  =============================
    //  = Select Input -            =
    //  =============================
    $wp_customize->add_setting('navbar__inner_container', [
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ]);

    $wp_customize->add_control('navbar__inner_container', [
        'type'        => 'select',
        'label'       => config('theme_options.navbar__inner_container.label'),
        // 'description' => __(''),
        'section'     => config('theme_options.navbar__inner_container.section'),
        'settings'    => 'navbar__inner_container',
        'choices'     => config('theme_options.navbar__inner_container.choices'),
    ]);

    //  =============================
    //  = Select Input -            =
    //  =============================
    $wp_customize->add_setting('navbar__color', [
        'default'        => config('theme_options.navbar__color.value'),
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ]);

    $wp_customize->add_control('navbar__color', [
        'type'        => 'select',
        'label'       => config('theme_options.navbar__color.label'),
        // 'description' => __(''),
        'section'     => config('theme_options.navbar__color.section'),
        'settings'    => 'navbar__color',
        'choices'     => config('theme_options.navbar__color.choices'),
    ]);

    //  =============================
    //  = Select Input -            =
    //  =============================
    $wp_customize->add_setting('navbar__background', [
        'default'        => config('theme_options.navbar__background.value'),
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ]);

    $wp_customize->add_control('navbar__background', [
        'type'        => 'select',
        'label'       => config('theme_options.navbar__background.label'),
        // 'description' => __(''),
        'section'     => config('theme_options.navbar__background.section'),
        'settings'    => 'navbar__background',
        'choices'     => config('theme_options.navbar__background.choices'),
    ]);

    //  =============================
    //  = Footer                    =
    //  =============================
    $wp_customize->add_section('footer', [
        'title'    => __('Footer', 'sage'),
        'description' => '',
        'priority' => 130,
    ]);

    //  =============================
    //  = Text Input - Footer class =
    //  =============================
    $wp_customize->add_setting('footer__class', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'theme_mod',
    ));

    $wp_customize->add_control('footer__class', array(
        'label'      => config('theme_options.footer__class.label'),
        'section'    => config('theme_options.footer__class.section'),
        'settings'   => 'footer__class',
    ));

    //  =============================
    //  = Head Section            =
    //  =============================
    $wp_customize->add_section('head', array(
        'title'    => __('Code Fields (Tracking etc.)', 'sage'),
        'description' => '',
        'priority' => 130,
    ));

        //  =============================
        //  = Text Input - Tracking     =
        //  =============================
        $wp_customize->add_setting('tracking', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'theme_mod',

        ));

        $wp_customize->add_control('control_tracking', array(
            'label'      => __('Tracking Code', 'sage'),
            'section'    => 'head',
            'settings'   => 'tracking',
            'type'       => 'textarea'
        ));

        //  =============================
        //  = Text Input - fonts        =
        //  =============================
        $wp_customize->add_setting('fonts', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'theme_mod',

        ));

        $wp_customize->add_control('control_fonts', array(
            'label'      => __('Fonts etc.', 'sage'),
            'section'    => 'head',
            'settings'   => 'fonts',
            'type'       => 'textarea'
        ));

        //  =============================
        //  = Text Input - Tracking     =
        //  =============================
        $wp_customize->add_setting('google_analytics', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'theme_mod',

        ));

        $wp_customize->add_control('control_google_analytics', array(
            'label'      => __('Google Analytics', 'sage'),
            'section'    => 'head',
            'settings'   => 'google_analytics',
            'type'       => 'textarea'
        ));

        //  =============================
        //  = Text Input - Tracking     =
        //  =============================
        $wp_customize->add_setting('facebook_pixel', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'theme_mod',

        ));

        $wp_customize->add_control('facebook_pixel', array(
            'label'      => __('Facebook Pixel', 'sage'),
            'section'    => 'head',
            'settings'   => 'facebook_pixel',
            'type'       => 'textarea'
        ));


    /*
    //  =============================
    //  = Social Media              =
    //  =============================
    $wp_customize->add_section('nps_section_social', array(
        'title'    => __('Social Media', 'sage'),
        'description' => '',
        'priority' => 120,
    ));

        //  =============================
        //  = Text Input - Facebook     =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[facebook]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_facebook', array(
            'label'      => __('Facebook URL', 'sage'),
            'section'    => 'nps_section_social',
            'settings'   => 'nps_theme_options[facebook]',
        ));

        //  =============================
        //  = Text Input - Twitter      =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[twitter]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_twitter', array(
            'label'      => __('Twitter URL', 'sage'),
            'section'    => 'nps_section_social',
            'settings'   => 'nps_theme_options[twitter]',
        ));

        //  =============================
        //  = Text Input - Instagram    =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[instagram]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_instagram', array(
            'label'      => __('Instagram URL', 'sage'),
            'section'    => 'nps_section_social',
            'settings'   => 'nps_theme_options[instagram]',
        ));

        //  =============================
        //  = Text Input - Snapchat     =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[snapchat]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_snapchat', array(
            'label'      => __('Snapchat URL', 'sage'),
            'section'    => 'nps_section_social',
            'settings'   => 'nps_theme_options[snapchat]',
        ));

        //  =============================
        //  = Text Input - Spotify      =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[spotify]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_spotify', array(
            'label'      => __('Spotify URL', 'sage'),
            'section'    => 'nps_section_social',
            'settings'   => 'nps_theme_options[spotify]',
        ));

        //  =============================
        //  = Text Input - YouTube      =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[youtube]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_youtube', array(
            'label'      => __('YouTube URL', 'sage'),
            'section'    => 'nps_section_social',
            'settings'   => 'nps_theme_options[youtube]',
        ));

    //  =============================
    //  = Contact Section           =
    //  =============================
    $wp_customize->add_section('nps_section_contact', array(
        'title'    => __('Contact Information', 'sage'),
        'description' => '',
        'priority' => 120,
    ));

        //  =============================
        //  = Text Input - Phone        =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[phone]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_phone', array(
            'label'      => __('Phone', 'sage'),
            'section'    => 'nps_section_contact',
            'settings'   => 'nps_theme_options[phone]',
        ));

        //  =============================
        //  = Text Input - Email        =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[email]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));

    $wp_customize->add_control('nps_text_email', array(
            'label'      => __('Email', 'sage'),
            'section'    => 'nps_section_contact',
            'settings'   => 'nps_theme_options[email]',
        ));

    //  =============================
    //  = Footer Section            =
    //  =============================
    $wp_customize->add_section('nps_section_footer', array(
        'title'    => __('Footer', 'sage'),
        'description' => '',
        'priority' => 120,
    ));

        //  =============================
        //  = Text Input - About        =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[about]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',

        ));

    $wp_customize->add_control('nps_text_about', array(
            'label'      => __('About', 'sage'),
            'section'    => 'nps_section_footer',
            'settings'   => 'nps_theme_options[about]',
            'type'       => 'textarea'
        ));

    //  =============================
    //  = Head Section            =
    //  =============================
    $wp_customize->add_section('nps_section_head', array(
        'title'    => __('Code Fields (Tracking etc.)', 'sage'),
        'description' => '',
        'priority' => 120,
    ));

        //  =============================
        //  = Text Input - Tracking     =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[tracking]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',

        ));

        $wp_customize->add_control('nps_tracking', array(
            'label'      => __('Tracking Code', 'sage'),
            'section'    => 'nps_section_head',
            'settings'   => 'nps_theme_options[tracking]',
            'type'       => 'textarea'
        ));

        //  =============================
        //  = Text Input - Tracking     =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[google_analytics]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',

        ));

        $wp_customize->add_control('nps_google_analytics', array(
            'label'      => __('Google Analytics', 'sage'),
            'section'    => 'nps_section_head',
            'settings'   => 'nps_theme_options[google_analytics]',
            'type'       => 'textarea'
        ));

        //  =============================
        //  = Text Input - Tracking     =
        //  =============================
        $wp_customize->add_setting('nps_theme_options[facebook_pixel]', array(
            'default'        => '',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',

        ));

        $wp_customize->add_control('nps_facebook_pixel', array(
            'label'      => __('Facebook Pixel', 'sage'),
            'section'    => 'nps_section_head',
            'settings'   => 'nps_theme_options[facebook_pixel]',
            'type'       => 'textarea'
        ));
        */
}

add_action('customize_register', __NAMESPACE__ . '\\customize_register');
